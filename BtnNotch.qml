import QtQuick 2.0
import QtQuick.Controls 2.13


    Rectangle {
        id: rectangle
        y: 184
        width: 110
        height: 35
        color: "#17b9af"
        radius: 10
        border.color: "#17b9af"
        border.width: 2
        z: 1

        Text {
            id: text1
            color: "#262626"
            text: "Notch "
            anchors.fill: parent
            font.pixelSize: 16
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            z: 2

       }


        Text {
            id: text2
            x: 8
            y: 20
            visible: false
            color: "#686614"
            text: qsTr("base")
            font.pixelSize: 12
        }
        states: [
            State {
                name: "off"
                when: !mouseArea.pressed && !mouseArea.containsMouse
                            PropertyChanges {
                                target: text1
                                color: "#17b9af"
                                text: "Notch OFF"


                            }
                            PropertyChanges {
                                target: rectangle
                                color: "#262626"


                            }

                            PropertyChanges {
                                target: text2
                                color: "#d8d438"
                                text: qsTr("off")
                            }

            },
            State {
                name: "hover"
                when: mouseArea.containsMouse && !mouseArea.pressed
                PropertyChanges {
                                target: rectangle
                                color: "#17b9af"
                                border.width: 3
                            }

                            PropertyChanges {
                                target: text2
                                color: "#e1dd23"
                                text: qsTr("hover")
                            }
            },
            State {
                name: "on"
                when:  mouseArea.pressed
                PropertyChanges {
                    target: rectangle
                    color: "#17b9af"

                }
                PropertyChanges {
                    target: text1
                    text: "Notch ON"
                    color: "#262626"

                }

                PropertyChanges {
                    target: text2
                    color: "#e4e041"
                    text: qsTr("on")
                }

            },
            State {
                name: "click"

                PropertyChanges {
                    target: text2
                    color: "#e0dc49"
                    text: qsTr("click")
                }
            }
        ]



        MouseArea {
            id: mouseArea
            anchors.fill: parent
            z: 3
            hoverEnabled: true

        }
    }



