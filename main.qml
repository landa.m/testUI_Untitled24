import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    width: 640
    height: 480
    visible: true


    Background {
        id: background
        anchors.fill: parent

        Header {
            id: header
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top

        }

        ToolBar {
            id: toolBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: header.bottom
            anchors.topMargin: 0
            anchors.rightMargin: 0
            anchors.leftMargin: 0
        }
        Image {
            id: gradOverlayBgr
            width: parent.width*0.86
            anchors.top: toolBar.bottom
            source: "gradOverlayBgr.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 0
            fillMode: Image.PreserveAspectFit

            Image {
                id: signalll
                anchors.fill: parent
                source: "signalll.svg"
                fillMode: Image.PreserveAspectFit
            }
        }


    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.66}
}
##^##*/
