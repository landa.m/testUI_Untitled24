import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Item {
    id: item1
    width: 1920
    height: 60
    Rectangle {
        id: rectangle
        color: "#212629"
        radius: 10
        border.width: 0
        anchors.fill: parent

        RowLayout {
            id: topLeftButt
            x: 0
            y: 0
            anchors.left: parent.left
            anchors.right: middleLogo.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 711
            Image {
                id: buu
                source: "buu.svg"
                Layout.leftMargin: 35
                Layout.margins: 2
                fillMode: Image.Stretch
                smooth: false
                cache: false
            }

            Text {
                id: text1
                color: "#c2c2c2"
                text: qsTr("EEG Recording")
                font.letterSpacing: 0.7
                verticalAlignment: Text.AlignVCenter
                font.weight: Font.Light
                font.family: "Arial"
                maximumLineCount: 1
                font.capitalization: Font.AllUppercase
                Layout.maximumWidth: 330
                minimumPixelSize: 16
                Layout.minimumWidth: 96
                Layout.maximumHeight: 400
                style: Text.Normal
                minimumPointSize: 16
                Layout.preferredWidth: 120
                Layout.minimumHeight: 10
                font.pointSize: 16
                textFormat: Text.PlainText
                Layout.margins: 1
                renderType: Text.NativeRendering
                Layout.preferredHeight: 18
            }
            Layout.fillHeight: true
            Layout.fillWidth: false
        }

        RowLayout {
            id: topRightButt
            x: 960
            y: 0
            anchors.left: middleLogo.right
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 0
            Image {
                id: settings
                source: "settings.svg"
                sourceSize.height: 26
                z: 2
                sourceSize.width: 26
                Layout.margins: 1
                fillMode: Image.PreserveAspectFit
                Layout.preferredWidth: 26
                Layout.maximumHeight: 50
                smooth: false
                Layout.maximumWidth: 50
                Layout.preferredHeight: 26
                Layout.minimumWidth: 20
            }

            Rectangle {
                id: rectangle1
                width: 170
                height: 40
                color: "#1affffff"
                radius: 10
                border.width: 0
                Layout.margins: 1
                Layout.preferredWidth: 170
                Layout.minimumHeight: 26
                Layout.preferredHeight: 36

                TextArea {
                    id: textArea
                    text: qsTr("Text Area")
                }
            }

            Image {
                id: minimize
                source: "minimize.svg"
                sourceSize.height: 58
                Layout.rowSpan: 1
                Layout.leftMargin: 2
                sourceSize.width: 60
                Layout.margins: 11
                fillMode: Image.PreserveAspectFit
                smooth: false
                Layout.minimumHeight: 20
                Layout.rightMargin: 2
                Layout.minimumWidth: 20
            }

            Image {
                id: maximize
                source: "maximize.svg"
                sourceSize.height: 58
                Layout.rowSpan: 1
                sourceSize.width: 60
                Layout.margins: 11
                fillMode: Image.PreserveAspectFit
                smooth: false
                Layout.minimumHeight: 26
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.minimumWidth: 26
            }

            Image {
                id: closeBtn
                source: "closeBtn.svg"
                sourceSize.height: 58
                Layout.rowSpan: 1
                sourceSize.width: 60
                Layout.margins: 11
                fillMode: Image.PreserveAspectFit
                smooth: false
                Layout.minimumHeight: 26
                Layout.rightMargin: 17
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.minimumWidth: 26
            }
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.rightMargin: 0
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.5}
}
##^##*/
