import QtQuick 2.0
import QtQuick.Layouts 1.0

Item {
    width: 1920
    height: 60

    Background {
        id: background
        anchors.fill: parent


        RowLayout {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 33

            spacing: 50


            RowLayout {
                Layout.fillHeight: false
                Layout.fillWidth: true
                spacing: 25


                Image {
                    id: bright
                    source: "/img/bright.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vectorohmnonsimple
                    source: "/img/Vectorohmnonsimple.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vecpie
                    source: "Vecp[ie.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vectoredit
                    source: "Vectoredit.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vector222nonsimple
                    source: "Vector222nonsimple.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vdector
                    source: "Vdector.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }

                Image {
                    id: vectogr
                    source: "Vectogr.svg"
                    Layout.fillHeight: false
                    Layout.fillWidth: true
                    smooth: false
                    fillMode: Image.PreserveAspectFit
                }
            }
            BtnNotch {
                id: btnNotch
                Layout.fillHeight: false
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter


            }

        }
    }




}


